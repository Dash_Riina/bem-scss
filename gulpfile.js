let gulp = require('gulp'),
    sass = require('gulp-sass')
    browserSync = require('browser-sync');

gulp.task('sass', function() {
    return gulp.src("./src/scss/**/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("./dist/css"));
});
gulp.task('copy-html', function () {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./dist'));
});
gulp.task('copy-js', function () {
    gulp.src('./src/js/**/*.js')
        .pipe(gulp.dest('./dist/js'));
});
gulp.task('copy-img', function () {
    gulp.src('./src/img/**/*.*')
        .pipe(gulp.dest('./dist/img'));
});
gulp.task('serve', ['sass','copy-html','copy-js','copy-img'], function() {

    browserSync.init({
        server: "./dist"
    });
    gulp.watch('./src/scss/**/*.scss', ['sass']).on('change', browserSync.reload);
    gulp.watch("./src/*.html", ['copy-html']).on('change', browserSync.reload);
    gulp.watch("./src/js/*.js", ['copy-js']).on('change', browserSync.reload);
});